"use strict"
let tabs = function () {

	let title = document.querySelectorAll('.tabs-title')
	let description = document.querySelectorAll('.tabs-description')
	let tabActive;

	title.forEach(item => {
		item.addEventListener('click', tabTitle)
	});

	function tabTitle() {
		title.forEach(item => {
			item.classList.remove('active')
		});
		this.classList.add('active')
		tabActive = this.getAttribute('data-tab-name');
		descriptionTab1(tabActive);

	}
	function descriptionTab1(tabActive) {
		description.forEach(item => {
			item.classList.contains(tabActive) ? item.classList.add('active') : item.classList.remove('active')
		})

	}

}


tabs()