// деструктуризация нужна для клонирование обьекта или масива, а не создания копии ссылки, также можно более удобно получать части массивов или обьектов и присваивать их в переменные.

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const ourClients = [...new Set(clients1.concat(clients2))];
/////////////////////////////////////// 


const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17, 
    gender: "woman",
    status: "human"
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human"
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire"
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire"
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire"
  }
];

const charactersShortInfo = JSON.parse(JSON.stringify(characters))

charactersShortInfo.forEach((e) => {
    delete e.status
    delete e.gender
})


//..............................................
const user1 = {
  name: "John",
  years: 30,
};
const { name, years, isAdmin = false } = user1;
const add = document.getElementById('add')
add.insertAdjacentHTML(
  'afterend',

  `<ul> user1
  <li>name : ${name}</li>
  <li> years : ${years}</li>
  <li> isAdmin : ${isAdmin}</li>
</ul> `
);
/////////////////////////////////////////////////

const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422, 
    lng: 139.876632
  }
}

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome'
}

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto', 
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05'
}
const fullProfile = Object.assign({}, satoshi2018, satoshi2019, satoshi2020);


///////////////////////////////////////////////

const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}
const alotOfBooks = [...new Set(books.concat(bookToAdd))];
////////////////////////////////////////////////////

const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

const bigEmployee = { ...employee, age: 22, salary:22000}
console.log(bigEmployee);

//////////////////////////////////////////

const array = ['value', () => 'showValue'];
const[value,showValue]= array
// Допишите ваш код здесь
//console.log(showValue);
//alert(value); // должно быть выведено 'value'
//alert(showValue());  // должно быть выведено 'showValue'



