// протопитное наследование служит нам для клонирования (свойств , методов) других обьекта, а не их копирования(переписавания), очевидным путем.

class Employee {
	constructor(name ,age, salary) {
		this._name  = name ;
		this._age  = age ;
		this._salary = salary;
	}
	get name () {
        return 'name' + this._name;
    }
    set name (_name) {
        this.name = _name;
	}
	get age () {
        return 'age' + this._age;
    }
    set age (_age) {
        this.age = _age;
	}
	get salary () {
        return 'salary' + this._salary;
    }
    set salary (_salary) {
        this.salary = _salary;
	}
}
const lang = {
	engl : true,
	ru: true,
}
class Programmer extends Employee {
	constructor(name, age, salary,lang) {
		super(name, age, salary);
		this.lang = lang
	}
	set salary(_salary) {
		 this.salary = _salary
		
	}
	get salary () {
        return this._salary * 2 ;
    }
}


const programmer = new Programmer('Vasya', 22, 22000,lang)
const programmer2 = new Programmer('Ivan', 22, 33000,lang)
const programmerNew = new Programmer('Ina', 44, 44000,lang)

console.log(programmer);
console.log(programmerNew);
console.log(programmerNew);

