//try...catch - уместно использовать в коде когда допускаем что будет ошиба , тот код ложим в try , отлавливаем ошибку в catch, так же можем ложить тот код,  который приходит нам с beckend.


const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];
const linkBooks = document.getElementById('root')
try {
	books.forEach(function (e) {

		if (e.name && e.price && e.author) {
			linkBooks.insertAdjacentHTML(
				'afterbegin',
	
				`<ul>  ${e.author}
				<li>${e.name}</li >
				<li>${e.price}</li >
				</ul > `
			);
		}	 
	});
	
}
catch (err) {
	console.log(err);
}
finally {
	books.forEach(function (e) {
		if (!(e.name && e.price && e.author)) {
			console.log(e);
		}
	});
}
