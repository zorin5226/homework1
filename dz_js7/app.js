"use strict";
//DOM - програмный интерфейс, который помогает нам получить доспуп к html, а такеже менять его структуру и содержимое.
function drawList(list) {
	const ul = document.createElement("ul");
	let li = ""
	list.map(item => {
		li += `<li>${item}</li>`;
	})

	ul.innerHTML = li
	document.body.append(ul);
}
drawList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"])