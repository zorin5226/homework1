// асинхронный код в js применяется в тех случиях, когда нужно дождаться результат выполнения функции или метода(например запрос на бек)


const btnGetIp = document.getElementById('button')
btnGetIp.addEventListener('click', getIp)

async function  getIp() {
  const ip = await fetch('https://api.ipify.org/?format=json')
  const ipUser = await ip.json()
  getInfoUser(ipUser)
}

async function getInfoUser(ipUser) {
  const userInfoSearch = await fetch(`http://ip-api.com/json/${ipUser.ip}?fields=continent,country,region,city,district`)
   const userInfo= await userInfoSearch.json()
  showInfoUser(userInfo)
}


function showInfoUser(userInfo) {
  const userInfoConteiner = document.getElementById('conteiner')
  for (const key in userInfo) {
    console.log(userInfo[key]);
  const p = document.createElement("p")
    p.innerText = key + ':' + userInfo[key]
    userInfoConteiner.append(p)
  }
  
  }

  
