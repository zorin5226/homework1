"use strict"
let tabs = function () {

	let servicesList = document.querySelectorAll('.services-list')
	let servicesContent = document.querySelectorAll('.services-content')
	let tabActive;

	servicesList.forEach(item => {
		item.addEventListener('click', tabTitle)
	});

	function tabTitle() {
		servicesList.forEach(item => {
			item.classList.remove('activ')
		});
		this.classList.add('activ')
		tabActive = this.getAttribute('data-tabs-name');
		descriptionTab1();
	}
	function descriptionTab1() {
		servicesContent.forEach(item => {
			item.classList.contains(tabActive) ? item.classList.add('activ') : item.classList.remove('activ')
		})

	}
}


tabs()
/////////////////////////////////////////////////////
const servicesTabs = () => {

	const servicesList = document.querySelectorAll('.catalog-link')
	const servicesContent = document.querySelectorAll('.catalog-option')
	const catalogmenu = document.getElementById('catalogMenu')
	const catalogButton = document.querySelectorAll('.catalog-button')
	const moreContent = document.querySelectorAll('.catalog-more')
	servicesList.forEach(item => {
		item.addEventListener('click', tabTitle)
	});


	function tabTitle() {
		servicesList.forEach(item => {
			item.classList.remove('catalog-link-active')
		});
		moreContent.forEach(item => {
			item.style.display = 'none'
		})
		if (catalogButton[0].style.display = 'none') {
			duttonsActive()
		}
		this.classList.add('catalog-link-active')
		const tabActive = this.getAttribute('data-catalog');
		descriptionTab1(tabActive);
	}
	function descriptionTab1(tabActive) {
		servicesContent.forEach(item => {
			item.classList.contains(tabActive) ? item.classList.add('catalog-option-active') : item.classList.remove('catalog-option-active')
		})
	}
	descriptionTab1('all')

	catalogButton[0].addEventListener('click', show)
	function show() {
		moreContent.forEach(element => {
			element.style.display = 'block'
		})
		catalogButton[0].style.display = 'none'
		catalogButton[1].style.display = 'block'

	}
	catalogButton[1].addEventListener('click', hide)
	function hide() {
		moreContent.forEach(element => {
			element.style.display = 'none'
		})
		duttonsActive()
	}
	function duttonsActive() {
		catalogButton[0].style.display = 'block'
		catalogButton[1].style.display = 'none'
	}
}
servicesTabs()
///////////////////////////////////////////////////////////

const peopeleKarusel = () => {
	const peopel = document.querySelectorAll('.item')
	const sliderDots = document.querySelectorAll(".slider-dot");
	const arrowPrev = document.querySelector('.arrow-prev')
	const arrowNext = document.querySelector('.arrow-next')
	let dotActive = ''
	sliderDots.forEach(e => {
		e.addEventListener('click', dotClick)
	})
	function dotClick(e) {
		sliderDots.forEach(e => {
			e.classList.remove('dot-active')
		})
		e.target.classList.add('dot-active')
		dotActive = e.target.getAttribute('data-speaker')
		peopelActive(dotActive)
	}
	function peopelActive(dotActive) {
		peopel.forEach(e => {
			e.classList.contains(dotActive) ? e.classList.add(`item-active`) : e.classList.remove(`item-active`)
		})
	}
	arrowPrev.addEventListener('click', minusSlide)
	arrowNext.addEventListener('click', plusSlide)
	let slideIndex = 0;
	function plusSlide() {
		showSlides(slideIndex += 1);
	}
	function minusSlide() {
		showSlides(slideIndex -= 1);
	}
	function showSlides(n) {
		let i;
		if (n > sliderDots.length) {
			slideIndex = 1
		}
		if (n < 1) {
			slideIndex = sliderDots.length
		}
		for (i = 0; i < sliderDots.length; i++) {
			sliderDots[i].classList.remove('dot-active')
		}
		sliderDots[slideIndex - 1].classList.add('dot-active');
		const imgActive = sliderDots[slideIndex - 1].getAttribute('data-speaker')
		peopelActive(imgActive)
	}
}
peopeleKarusel()
