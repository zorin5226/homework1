/* 1: Если нам надо задавать один и тот же код в разных частях програмы, мы можем поместить этот код в функцию и вызывать непосредственно функцию*/
// 2: аргументы применяются  для передачи значения при вызове функции,(аргументы так же как и параметры могут не указываться )
function prom() {
	let condition;
	let operations;
	do {
		const number1 = prompt("Введите первое число")
		const number2 = prompt("Введите вторе число")
		operation = prompt("Enter operation (+ or - or * or / or **)");

		condition =
			!isNaN(+number1) &&
			number1 !== null &&
			number1 !== "" &&
			!isNaN(+number2) &&
			number2 !== null &&
			number2 !== "";

		operations =
			operation === "+" ||
			operation === "-" ||
			operation === "*" ||
			operation === "/" ||
			operation === "**";

		if (condition && operations) {
			switch (operation) {
				case "+":
					console.log(
						` Результат: ${+number1 + +number2}.`
					);
					break;
				case "-":
					console.log(
						` Результат: ${number1 - number2}.`
					);
					break;
				case "/":
					console.log(
						` Результат: ${number1 / number2}.`
					);
					break;
				case "*":
					console.log(
						` Результат: ${number1 * number2}.`
					);
					break;
				case "**":
					console.log(
						` Результат: ${number1 ** number2}.`
					);
					break;
			}
		} else {
			alert("eroor")
		}
	} while (!condition && !operations);

}
prom()
