"use strict"
//обработчик событий - это то что пользователь делает на странице например : click, scroll и тд...
function price() {
	const result = document.getElementById('result');
	const input = document.getElementById('input');
	const span = document.getElementById('span');
	const button = document.getElementById('button');
	const error = document.getElementById('error');


	input.onblur = function (el) {
		if (!el.target.value.length || +el.target.value < 0) {
			error.classList.add('active')
			el.target.value = ""
			this.classList.add("error-input")
			result.classList.remove('result-active')
		} else {
			span.innerHTML = 'текущая цена' + ':' + `${el.target.value}`
			result.classList.add("result-active")
			el.target.value = ""
		}

	}

	button.onclick = function () {
		result.classList.remove('result-active')
		input.value = ""
	}
	input.onfocus = function () {
		this.classList.remove("error-input")
		error.classList.remove('active')
		input.classList.add('focus-activ')
		result.classList.remove('result-active')
	}


};
console.log(typeof input.value);
price()

