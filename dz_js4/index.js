"use strict";

const createNewUser = function () {
	let firstName = prompt("Введите имя");
	let lastName = prompt("Введите фамилию");
	return {
		setFirstName: function (value = "") {
			if (value.length > 0) {
				firstName = value;
			}
		},
		setLastName: function (value = "") {
			if (value.length > 0) {
				lastName = value;
			}
		},
		getLogin: function () {
			const result = firstName.toLowerCase()[0] + lastName.toLowerCase();
			return result;
		}
	}
}
const newUser = createNewUser();
newUser.setFirstName();
console.log(newUser.getLogin());
