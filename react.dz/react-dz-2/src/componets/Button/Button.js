import React from "react";
import PropTypes from 'prop-types'

const Button = (props) => {
	const { text, onClick, backgroundColor } = props
	return (
		<button className={`wrapper-btn `}
			onClick={onClick}
			//id={id}
			style={{backgroundColor : backgroundColor}}
		>
			{text}
			
		</button>
	)
}
Button.propTypes = {
	backgroundColor : PropTypes.string,
	text: PropTypes.string,
	onClick: PropTypes.func,
  }
export default Button