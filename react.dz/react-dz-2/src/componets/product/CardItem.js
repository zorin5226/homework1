import React,{useEffect, useState} from "react";
import Button from "../Button/Button";
import { ReactComponent as Svg } from "./svg.svg"
import Modal from "../modal/modal";
import PropTypes from "prop-types";


const CardItem = ({ element }) => {
	let [isActive, setIsActive] = useState(false)
	const [modal, setModal] = useState(false)

	const handleToggle = () => {
		element.isInCart = element.isInCart === 'false' ? element.isInCart = 'true' : element.isInCart = 'false'
		setIsActive(element.isInCart === 'false' ? isActive = false : isActive = true)
		
		element.isInCart === 'true' ? localStorage.setItem(`${element.id}`, JSON.stringify(element)) : localStorage.removeItem(`${element.id}`)
		
	};
	useEffect(() => {
		if (localStorage.getItem(`${element.id}`) !== null) {
			setIsActive(true)
		}
	}, [element.id])

	return(
		<div
			className="content__item"
			key={element.id}
			id={element.id}
		>
		<Modal
        	isOpen={modal}
        	title={isActive ? "Delete from cart?" : "Add to cart?" }
			closeModal={() => setModal(false)}
			addCart={()=>handleToggle(element.id)}

        
		/>
			


			<div className="content__img-conteiner" >
				<img
					className="content__img"
					src={element.url}
					alt={'telephon'}
				/>
			</div>
			<div className="content__item-content">
				<div className="content__item-info">
					<p>{element.name}</p>
					<p>{element.price}</p>
					<span
						style={{
							backgroundColor: `${element.color}`,
							borderRadius: `100%`,
							width: `20px`,
							height: `20px`
						}}
					/>
				</div>
				<div className="content__item-buttons" >
					<Svg
						id={element.id}
						className={ isActive ? 'content__item-star active' : 	'content__item-star'}
						onClick={() => handleToggle()}
					/>
					<Button
						text={(localStorage.getItem(`${element.id}`) !== null) ? "Delete from cart" : "Add to cart"}
						onClick={() => setModal(true)  }
					/>

				</div>
				

			
			</div>

			<div className="content__btn-conteiner" ></div>
		</div>
	)
}
CardItem.propTypes = {
	element: PropTypes.object,
  }
export default CardItem