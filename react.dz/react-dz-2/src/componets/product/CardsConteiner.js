import React from "react";
import "./cards.scss"
import Card from "./Cards";
import PropTypes from"prop-types"

function Content({cards}) {

	//const newArr = cards.map((e) => e)
	
	return (

		<div className="content__conteiner" >
			{
				<Card arr={cards}/>
				
			}
			
		</div>
	)
	

}
Content.propTypes = {	
	cards : PropTypes.array
  }
export default Content