import React, { useEffect, useState } from "react";
//import Button from "./componets/button";
import './index.css'
//import Api from "./Api"
import Content from "./componets/product/CardsConteiner";



function App() {
  const [arrProduct, setArrProduct] = useState([])

  useEffect(() => {
    fetch('http://localhost:3000/Data.json ')
    .then(response => response.json())
      .then(data => {
        setArrProduct(data.products)
        return  data
      })

  }, [])
  
  return (
    <div className="wrapper">
      <Content
        cards={arrProduct}
      />

    </div>
  );
}

export default App;
