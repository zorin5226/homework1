import React from 'react'
import PageHome from '../PageHome/PageHome';

const PageFavorites = ({ cards }) => {
	
	const lcstrg = JSON.parse(localStorage.getItem('isInFavorites')) || []
	let cardsInFavorites =[]
	//
	for (const i in lcstrg) {
		for (const key in cards) {
			if(cards[key].id === lcstrg[i] )
			cardsInFavorites.push(cards[key])
		}
	}
	return (
		<>
		
			<h1>favorites</h1>
			<PageHome cards={cardsInFavorites} />
			
		</>
	)
}
export default PageFavorites