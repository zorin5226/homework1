import React from "react";
import "./cards.scss"
import Cards from "./Cards";
import PropTypes from"prop-types"

function PageHome({cards}) {

	//const newArr = cards.map((e) => e)
	
	
	return (

		<div className="content__conteiner" >
			{
				<Cards arr={cards}/>
				
			}
			
		</div>
	)
	

}
PageHome.propTypes = {	
	cards : PropTypes.array
  }
export default PageHome