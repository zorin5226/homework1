import React,{useEffect, useState} from "react";
import Button from "../Button/Button";
import { ReactComponent as Svg } from "./svg.svg"
import Modal from "../modal/modal";
import PropTypes from "prop-types";

const CardItem = ({ element }) => {
	let [isInCart, setIsInCart] = useState(false)
	let [isInFavorites, setIsInFavorites] = useState(false)
	const [modal, setModal] = useState(false)

	const handleToggleCart = () => {
		setIsInCart(!isInCart)
		let storageCartId = JSON.parse(localStorage.getItem('isInCart')) || [];
		activeLocal(storageCartId,'isInCart')

	};
	const handleToggleFavorites = () => {
		setIsInFavorites(!isInFavorites)
		let storageFavoritesId = JSON.parse(localStorage.getItem('isInFavorites')) || [];
		activeLocal(storageFavoritesId,'isInFavorites')
	};

	const activeLocal = (store,key) => {
		
		if (store.includes(element.id)) {
			const rez =  store.filter(item => item !== element.id)
			localStorage.setItem(`${key}`, JSON.stringify(rez))
		} else {
			store.push(element.id)
			localStorage.setItem(`${key}`, JSON.stringify(store))
		}
	}
	
	useEffect(() => {
		if (JSON.parse(localStorage.getItem('isInCart'))) {
			JSON.parse(localStorage.getItem('isInCart')).forEach((e) => {
				if (element.id === e) {
					setIsInCart(true)
				}
			})
		}
		if (JSON.parse(localStorage.getItem('isInFavorites'))) {
			JSON.parse(localStorage.getItem('isInFavorites')).forEach((e) => {
				if (element.id === e) {
					setIsInFavorites(true)
				}
			})
		}
	},[element.id])
	return(
		<div
			className="content__item"
			key={element.id}
			id={element.id}
		>
		<Modal
        	isOpen={modal}
        	title={isInFavorites ? "Delete from cart?" : "Add to cart?" }
			closeModal = {() => setModal(false)}
			addFavorites = {()=> handleToggleFavorites()}

        
		/>
			


			<div className="content__img-conteiner" >
				<img
					className="content__img"
					src={element.url}
					alt={'telephon'}
				/>
			</div>
			<div className="content__item-content">
				<div className="content__item-info">
					<p>{element.name}</p>
					<p>{element.price}</p>
					<span
						style={{
							backgroundColor: `${element.color}`,
							borderRadius: `100%`,
							width: `20px`,
							height: `20px`
						}}
					/>
				</div>
				<div className="content__item-buttons" >
					<Svg
						id={element.id}
						className={ isInCart ? 'content__item-star active' : 	'content__item-star'}
						onClick={() => handleToggleCart()}
					/>
					<Button
						text={isInFavorites ? "Delete from cart" : "Add to cart"}
						onClick={() => {
							setModal(true)
						}}
					/>

				</div>
				

			
			</div>

			<div className="content__btn-conteiner" ></div>
		</div>
	)
}
CardItem.propTypes = {
	element: PropTypes.object,
  }
export default CardItem