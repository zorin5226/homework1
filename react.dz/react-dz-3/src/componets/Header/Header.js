import React from 'react'
import "./Header.scss"
import {NavLink} from 'react-router-dom'
//import PageFavorites from"../PageFavorites/PageFavorites"
//import PageCart from"../PageCart/PageCart"

const Header = () => {
	return (
		<header className='header'>
			<nav className='header__nav'>
				<ul className='header__items'>
					<li className='header__item' >
						<NavLink	className='header__item-link' to='/'>Главная</NavLink>
					</li>
					<li className='header__item' >
						<NavLink	className='header__item-link' to='../PageFavorites'>Корзина </NavLink>
					</li>
					<li className='header__item' >
						<NavLink	className='header__item-link' to='../PageCart'>Избранное</NavLink>
					</li>
				</ul>
			</nav>
		 </header>
	 )
}
 export default Header