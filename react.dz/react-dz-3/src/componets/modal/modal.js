import React  from 'react'
import './modal.scss'
import Button from '../Button/Button'
import PropTypes from 'prop-types'

const Modal = (props) => {
	const { isOpen, title, closeModal, addFavorites } = props
	return (
		
		<div
			className={`modal ${isOpen ? 'open' : null}`}
			onClick={e => e.target.className === 'modal open' ? closeModal() : null}
		>

			<div className='modal__dialog '>
				<div className='modal__header'>
					<h3 className='modal__title'>{title}</h3>
					<span className='modal__close' onClick={()=> closeModal()}>
						&times;
					</span>
				</div>
				<div className='modal__body'>
				</div>				
				<div className='modal__footer'>
					<div className="modal__footer-btns">
            <Button
				text={'Ok'}
					onClick={() => {
						addFavorites()
						closeModal()
					
					}}
              
              backgroundColor={"green"}

            />
            <Button
              text={'Cansel'}
              onClick={() => closeModal()}
              
              backgroundColor={"red"}
            />

            
        	  </div>
				</div>
			</div>
		</div>
		
	)
}
Modal.propTypes = {
	isOpen: PropTypes.bool,
	title: PropTypes.string,
	closeModal: PropTypes.func,
	addCart: PropTypes.func
  }
export default Modal