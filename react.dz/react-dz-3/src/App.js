import React, { useEffect, useState } from "react";
import './index.css'
import PageHome from "./componets/PageHome/PageHome";
import Header from "./componets/Header/Header";
import { Route, Routes } from 'react-router-dom'
import PageFavorites from"./componets/PageFavorites/PageFavorites"
import PageCart from"./componets/PageCart/PageCart"




function App() {
  const [arrProduct, setArrProduct] = useState([])

  useEffect(() => {
    fetch('http://localhost:3000/Data.json ')
    .then(response => response.json())
      .then(data => {
        setArrProduct(data.products)
        return  data
      })

  }, [])
  
  return (
    <div className="wrapper">

      <Header />
      <Routes>
        <Route path="/" element={<PageHome cards={arrProduct}/>} />
        <Route path="/PageFavorites" element={<PageFavorites cards={arrProduct} />} />
        <Route path="/PageCart" element={<PageCart cards={arrProduct} />} />
      </Routes>

    </div>
  );
}

export default App;
