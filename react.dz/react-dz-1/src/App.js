import React, { useState } from "react";
import Button from "./componets/button";
import Modal from "./componets/modal";
import './index.css'

function App() {
  
  const [modal, setModal] = useState({
    modal1: false,
    modal2: false,
  })
  
  return (
    
    <div className="wrapper">
      <Modal
        isOpen={modal.modal1}
        header={"title 1"}
        closeModal={() => setModal(false)}
        
        action={
          <div className="modal__footer-btns">
            <Button
              text={'Ok'}
              onClick={() => setModal(false)}
              
              backgroundColor={"green"}

            />
            <Button
              text={'Cansel'}
              onClick={() => setModal(false)}
              
              backgroundColor={"red"}
            />

            
          </div>
        }
        
      />
      <Button
        text={'Open first modal'}
        onClick={() => setModal({ ...modal, modal1: true })}
        backgroundColor={"#cc1"}
        
      />

      <Modal
        isOpen={modal.modal2} 
        header={"title 2"}
        closeModal={() => setModal(false)}
        action={
          <div className="modal__footer-btns" >
            <Button
              text={'Ok'}
              onClick={() => setModal(false)}
              //id={}
              backgroundColor={"green"}

            />
            <Button
              text={'Cansel'}
              onClick={() => setModal(false)}
              //id={}
              backgroundColor={"red"}
            />

            
          </div>
        }
      />
      <Button
        text={'Open second modal'}
        onClick={() => setModal({ ...modal, modal2: true })}
        //id={}
        backgroundColor={"#ccc"}
      />

    </div>
  );
}

export default App;
