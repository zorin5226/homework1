import React  from 'react'
//import ReactDOM from 'react-dom'
import './modal.scss'

const Modal = (props) => {
	let { isOpen, header, closeModal, action, } = props
	

	return (
		
		<div
			className={`modal ${isOpen ? 'open' : null}`}
			onClick={e => e.target.className === 'modal open' ? closeModal() : null}
		>
			<div className='modal__dialog '>
				<div className='modal__header'>
					<h3 className='modal__title'>{header}</h3>
					<span className='modal__close' onClick={()=> closeModal()}>
						&times;
					</span>
				</div>
				<div className='modal__body'>
				</div>				
					<div className='modal__footer'>{action}</div>
			</div>
		</div>
		
	)
}
export default Modal