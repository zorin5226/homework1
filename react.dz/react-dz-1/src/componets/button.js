import React from "react";

const Button = (props) => {
	const{text , onClick, backgroundColor} = props
	return (
		<button className="wrapper-btn"
			onClick={onClick}
			//id={id}
			style={{backgroundColor : backgroundColor}}
		>
			{text}
			
		</button>
	)
}
export default Button