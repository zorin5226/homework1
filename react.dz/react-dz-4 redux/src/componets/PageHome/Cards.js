import React from "react"; 
import CardItem from "./CardItem";
import Modal from "../modal/modal";
//import PropTypes from"react-dom"


const Cards = ({arr}) => {
	return (
		< >
			< Modal />

			{arr.map(element => (
				<CardItem
					key={element.id}
					element={element}
					
				/>
			))}
		
		</>
		
	)
}
//Cards.propTypes = {
//	arr : PropTypes.object
//  }

export default Cards