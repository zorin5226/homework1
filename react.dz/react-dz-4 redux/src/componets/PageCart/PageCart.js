
import PageHome from '../PageHome/PageHome';

const PageCart = ({ cards }) => {

	const res = cards.filter(i => i.isInCart === true)
	if (res.length === 0) return <h2>'В корзине нет товаров</h2>	

	return (
		<>
			
			<PageHome cards={res} />	
		</>
	)
}
export default PageCart