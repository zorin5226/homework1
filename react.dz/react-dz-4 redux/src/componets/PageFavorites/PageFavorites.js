import React from 'react'
import PageHome from '../PageHome/PageHome';

const PageFavorites = ({ cards }) => {
	//const toggleFavotite = useSelector(state => state.favorites.data)
	//const [isActiveIds, setIsActiveIds] = useState([]);
	//useEffect(() => {
	//	setIsActiveIds(JSON.parse(localStorage.getItem('isInFavorites')) || [])
	//}, [toggleFavotite])
	//const cardInFavorite = cards.filter(item => isActiveIds.includes(item.id)) 
	//console.log(cardInFavorite.length,'cardInFavorite');
	const res = cards.filter( item => item.isInFavorite === true)
	
	return (
		<>
			
				 <h2>{res.length === 0  ? 'В избраном нет товаров' : ''}</h2>
				
				<PageHome cards={res} />
				
			
			
		</>
	)
}
export default PageFavorites