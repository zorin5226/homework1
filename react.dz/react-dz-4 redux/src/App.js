import React, { useEffect } from "react";
import './index.css'
import PageHome from "./componets/PageHome/PageHome";
import Header from "./componets/Header/Header";
import { Route, Routes } from 'react-router-dom'
import PageFavorites from"./componets/PageFavorites/PageFavorites"
import PageCart from "./componets/PageCart/PageCart"
import { getProductAC } from "./store/actionsCreator/productAC";
import { useDispatch, useSelector } from "react-redux";




function App() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getProductAC())
  }, [dispatch])

  const arrProduct = useSelector(state => state.product.data)

  return (
    <div className="wrapper">

      <Header />
      <Routes>
        <Route path="/" element={<PageHome cards={arrProduct}/>} />
        <Route path="/PageFavorites" element={<PageFavorites cards={arrProduct} />} />
        <Route path="/PageCart" element={<PageCart cards={arrProduct} />} />
      </Routes>

    </div>
  );
}

export default App;
