import { combineReducers } from "redux"
import product from './product'
import toggleModal from './toggleModal'

const rootReducers = combineReducers({
	product,
	modal:toggleModal,
	
})
export default rootReducers