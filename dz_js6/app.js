"use strict";
//Метод forEach() выполняет указанную функцию один раз для каждого элемента в массиве.
function filterBy(array, type) {
	return array.filter(item => {
		return typeof item !== typeof type
	})
}
console.log(filterBy(['hello', 'world', 23, 30, 40, '23', null, undefined], "string"));
