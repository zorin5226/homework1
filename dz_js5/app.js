"use strict";

const createNewUser = function () {
	let firstName = prompt("Введите имя");
	let lastName = prompt("Введите фамилию");
	let birthday = prompt("Введите день рождения НАПРИМЕР:13.03.1992 ");
	return {
		setFirstName: function (value = "") {
			if (value.length > 0) {
				firstName = value;
			}
		},
		setLastName: function (value = "") {
			if (value.length > 0) {
				lastName = value;
			}
		},
		getLogin: function () {
			const result = firstName.toLowerCase()[0] + lastName.toLowerCase();
			return result;
		},
		getAge: function () {
			const birthdayArr = birthday.split('.');
			const birthdayDate = new Date(+birthdayArr[2], +birthdayArr[1], +birthdayArr[0]);
			const actualDate = new Date();
			return Math.floor(+((actualDate.getTime() - birthdayDate) / (24 * 3600 * 365 * 1000)));
		},
		getPassword: function () {
			const birthdayArr = birthday.split('.');
			const birthdayDate = new Date(+birthdayArr[2], +birthdayArr[1], +birthdayArr[0]);
			const year = birthdayDate.getFullYear()
			return firstName.toUpperCase()[0] + lastName.toLowerCase() + year
		}
	}
}

const newUser = createNewUser();
newUser.setFirstName("Uasya");
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());