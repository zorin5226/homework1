const link = "https://ajax.test-danit.com/api/swapi/films";

async function getFilms(url) {
  let response = await fetch(url)
  response = await response.json();
  return response; 
}
getFilms(link).then((e) => {
  const linkFilms = document.getElementById('root')
  e.forEach((item) => {
    let id = item.episodeId
    getFimlCharacters(item.characters).then((charactersList) => {
      renderCharacters(charactersList,id)
    })
    linkFilms.insertAdjacentHTML('beforeend',
      `<ul id ="${item.id}"> ${item.name}
    <li>episode_id : ${item.episodeId}</li>
    <li> Title : ${item.director}</li>
    <li> Opening_crawl : ${item.openingCrawl.slice(0, 100)}...</li>
    </ul>
    `)
  })
})
function getFimlCharacters(links) {
  return new Promise((resolve, reject) => {
    const characters = []
    links.forEach((link) => {
      fetch(link)
        .then((response) => response.json())
        .then((character) => {
          characters.push(character.name)
          if (characters.length === links.length) {
            resolve(characters)
          }
        })
      
    })

  })
  
}
function renderCharacters(charactersList, id) {
  const episode = document.getElementById(id)
  const ul = document.createElement('ul')
  charactersList.forEach((character) => {
    const li = document.createElement('li')
    li.innerText = character;
    ul.appendChild(li)

  })
  episode.appendChild(ul)
}
